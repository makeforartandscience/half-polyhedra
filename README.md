# Half polyhedra

[![EN](images/en.png)](README.md) [![FR](images/fr.png)](README-fr.md)

a half polyhedron is a cut of a regular polyhedron in two identical parts.

[**stl**](stl) folder contains all 3D files of half polyhera you can see below. 
Cube side length is 50mm and all parts have same volume.

[**net**](net) folder contains a half tetrahedron net to be cut in paper or cardboard.

## half tetrahedron
[![half tetrahedron A](images/tetrahedron_a.png)](stl/tetrahedron_a.stl)
[![half tetrahedron B](images/tetrahedron_b.png)](stl/tetrahedron_b.stl)

## half cube
[![half cube A](images/cube_a.png)](stl/cube_a.stl)
[![half cube B](images/cube_b.png)](stl/cube_b.stl)
[![half cube C](images/cube_c.png)](stl/cube_c.stl)
[![half cube D](images/cube_d.png)](stl/cube_d.stl)

## half octahedron
[![half octahedron A](images/octahedron_a.png)](stl/octahedron_a.stl)
[![half octahedron B](images/octahedron_b.png)](stl/octahedron_b.stl)
[![half octahedron C](images/octahedron_c.png)](stl/octahedron_c.stl)

## half dodecahedron
[![half dodecahedron A](images/dodecahedron_a.png)](stl/dodecahedron_a.stl)
[![half dodecahedron B](images/dodecahedron_b.png)](stl/dodecahedron_b.stl)
[![half dodecahedron C](images/dodecahedron_c.png)](stl/dodecahedron_c.stl)

## half icosahedron
[![half icosahedron A](images/icosahedron_a.png)](stl/icosahedron_a.stl)
[![half icosahedron B](images/icosahedron_b.png)](stl/icosahedron_b.stl)
[![half icosahedron C](images/icosahedron_c.png)](stl/icosahedron_c.stl)

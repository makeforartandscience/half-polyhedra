# Demi polyèdres

[![EN](images/en.png)](README.md) [![FR](images/fr.png)](README-fr.md)

Nous appelerons demi polyèdre une section d'un polyèdre régulier en deux solides identiques.

le dossier [**stl**](stl) contient les fichiers3D des demi polyèdres.
Le cube a 50mm de côté et tous les solides ont le même volume.

Le dossier [**net**](net) contient les patrons des demi polyèdres en version dxf, svg et pdf.


## demi tétraèdre
![half tetrahedron A](images/tetrahedron_a.png)
![half tetrahedron B](images/tetrahedron_b.png)

[![half tetrahedron A](images/fichier_3d.png)](stl/tetrahedron_a.stl)
[![half tetrahedron B](images/fichier_3d.png)](stl/tetrahedron_b.stl)

[![half tetrahedron A](images/patron.png)](net/tetrahedron_a.pdf)
[![half tetrahedron B](images/patron.png)](net/tetrahedron_b.pdf)


## demi cube
![half cube A](images/cube_a.png)
![half cube B](images/cube_b.png)
![half cube C](images/cube_c.png)
![half cube D](images/cube_d.png)

[![half cube A](images/fichier_3d.png)](stl/cube_a.stl)
[![half cube B](images/fichier_3d.png)](stl/cube_b.stl)
[![half cube C](images/fichier_3d.png)](stl/cube_c.stl)
[![half cube D](images/fichier_3d.png)](stl/cube_d.stl)

[![half cube A](images/patron.png)](net/cube_a.pdf)
[![half cube B](images/patron.png)](net/cube_b.pdf)
[![half cube C](images/patron.png)](net/cube_c.pdf)
[![half cube D](images/patron.png)](net/cube_d.pdf)

## demi octaèdre
![half octahedron A](images/octahedron_a.png)
![half octahedron B](images/octahedron_b.png)
![half octahedron C](images/octahedron_c.png)

[![half octahedron A](images/fichier_3d.png)](stl/octahedron_a.stl)
[![half octahedron B](images/fichier_3d.png)](stl/octahedron_b.stl)
[![half octahedron C](images/fichier_3d.png)](stl/octahedron_c.stl)

[![half octahedron A](images/patron.png)](net/octahedron_a.pdf)
[![half octahedron B](images/patron.png)](net/octahedron_b.pdf)
[![half octahedron C](images/patron.png)](net/octahedron_c.pdf)

## demi dodecaèdre
[![half dodecahedron A](images/dodecahedron_a.png)](stl/dodecahedron_a.stl)
[![half dodecahedron B](images/dodecahedron_b.png)](stl/dodecahedron_b.stl)
[![half dodecahedron C](images/dodecahedron_c.png)](stl/dodecahedron_c.stl)

## demi icosaèdre
[![half icosahedron A](images/icosahedron_a.png)](stl/icosahedron_a.stl)
[![half icosahedron B](images/icosahedron_b.png)](stl/icosahedron_b.stl)
[![half icosahedron C](images/icosahedron_c.png)](stl/icosahedron_c.stl)
